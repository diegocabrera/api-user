'use strict'

const mongoose = require('mongoose')
const Usuarios = require('../models/usuarios')
const service = require('../servicios')

function signUp (req, res){
    const usuario = new Usuarios({
        correo: req.body.correo,
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        username: req.body.usuario,
        contraseña: req.body.contraseña
    })

    usuario.save((err)=>{
        if (err) return res.status(500).send({mensaje: `No se puedo registrar el usuario ${err}`})

        return res.status(200).send({token:service.crearToken(usuario)})
    })
}

function signIn (req, res){
    Usuarios.fin({ email: req.body.email}, (err,usuario)=>{
        if (err) return res.status(500).send({mensaje: `No se pudo acceder al usuario ${err}`})
        if (!usuario) return res.status(404).send({mensaje: `El usuario ${email} no existe`})
        req.usuario = usuario
        res.status(200).send({mensaje:`Te has logueado correctamente`, token: service.crearToken(usuario)})
    })
}

module.exports = {
    signUp,
    signIn
}
