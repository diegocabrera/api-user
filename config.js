module.exports = {
    port: process.env.PORT || 3000,
    db: process.env.MONGODB || 'mongodb://localhost:27017/api',
    secret_token: 'M1Sup3rT0k3nS3cr3t0'

}
