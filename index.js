'use strict'
const config = require('./config');
const mongoose = require('mongoose')
const app = require('./app')


mongoose.connect(config.db, (err, res)=>{
    if (err) {
        return console.log('No se pudo conectar con la base de Datos...')
    }
    app.listen(config.port, ()=>{
        console.log(`El servidor esta corriendo en el puerto ${config.port}`)
    })
})
