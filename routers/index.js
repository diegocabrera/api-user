'use strict'

const express = require('express')
const UsuarioCntrl = require('../controllers/usuario')
const auth = require('../middleware/auth')
const api = express.Router()


api.post('/signup', UsuarioCntrl.signUp)
api.post('/signin', UsuarioCntrl.signIn)
api.get('/privado', auth, function(req, res) {
    res.status(200).send({mensaje: 'El usuario tienes acceso'})
})

module.exports = api
