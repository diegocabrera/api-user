'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const config = require('../config')

function crearToken (usuario) {
    const playload = {
        sub: usuario._id,
        iat: moment().unix(),
        exp: moment().add(14, 'days').unix()
    }

    return jwt.encode(playload, config.secret_token)
}

function decodeToken (token) {
    const decoded = new Promise((resolve, reject)=>{
        try {
            const playload = jwt.decode(token, config.secret_token)

            if (playload.exp >= moment().unix()) {
                reject({
                    status: 401,
                    mensaje: `El token enviado ya ha caducado.`
                })
            }
            resolve(playload.sub)
        } catch (e) {
            reject({
                status: 500,
                mensaje: `Token Invalido`
            })
        }
    })
    return decoded
}

module.exports = {
    crearToken,
    decodeToken
}
